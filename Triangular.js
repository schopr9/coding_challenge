function solution(A) {
  let sum = 0;
  A.sort((a, b) => a - b);
  for (let i = 0; i < A.length - 2; i++) {
    if (
      A[i] + A[i + 1] > A[i + 2] &&
      A[i] + A[i + 2] > A[i + 1] &&
      A[i + 1] + A[i + 2] > A[i]
    ) {
      return 1;
    }
  }
  return sum;
}

console.log(solution([-2, -2, -2]));
