function getChange(M, P) {
  let changeArray  = [1, 5, 10, 25, 50]

  let response = Array(6).fill(0)

  let returnMoney = (M - P).toFixed(2).split('.')
  response[5] = parseInt(returnMoney[0])

  let cents = parseInt(returnMoney[1])

  for (let i = changeArray.length - 1; i >= 0; i--) {
    response[i] = Math.floor(cents / changeArray[i])
    cents = cents % changeArray[i]
  }
  return response
}


// console.log(getChange(5, 0.99))
console.log(getChange(3.14, 1.99) ) // should return [0,1,1,0,0,1]
console.log(getChange(3, 0.01))  // should return [4,0,2,1,1,2]
console.log(getChange(4, 3.14))  // should return [1,0,1,1,1,0]
console.log(getChange(0.45, 0.34))  // should return [1,0,1,0,0,0]