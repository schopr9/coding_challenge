/**
 * Emma will get an array of clouds numbered  if they are safe or  if they must be avoided. For example,  indexed from . The number on each cloud is its index in the list so she must avoid the clouds at indexes  and . She could follow the following two paths:  or . The first path takes  jumps while the second takes .
 * if 0 then jump
 * can jump +1 or +2
 *
 * @param {Array} c
 */
function jumpingOnClouds(c) {
  let jump = 0;
  if (c[0] === 1) return jump;
  let start = 0;
  let end = c.length - 1;
  while (start <= end) {
    if (start === end) return jump;
    if (c[start + 2] === 0) {
      start += 2;
    } else if (c[start + 1] === 0) {
      start++;
    }
    jump++;
  } 
  return jump;
}

there

