function solution(S) {
  let input = S.replace(/-/g, '').replace(/ /g, '')
  if (input.length % 3 === 1 ) {
    const newInput = input.slice(0, input.length - 4)
    const lastFour = input.slice(input.length - 4, input.length)
    console.log({ newInput, lastFour })
    const firstHalf = !!newInput && newInput.match(/\d{1,3}/g).join("-")
    const secondHalf = !!lastFour && lastFour.match(/\d{1,2}/g).join("-")
    return firstHalf ? `${firstHalf}-${secondHalf}` : secondHalf
  }
  return input.match(/\d{1,3}/g).join("-")
}

console.log(solution('00-44000000'))

// // 2 00
// // 3 000
// 4 00-00
// // 5 000-00
// // 6 000-000
// 7 000-00-00
// // 8 000-000-00
// // 9 000-000-000

// // length % 3 === 0
// // length % 3 === 2

// // legnth % 3 === 1

