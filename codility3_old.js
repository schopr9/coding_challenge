const WEEK_DAYS = [
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
  "Sunday",
];

const MONTHS = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

function getMonthDays(Y, monthIndex) {
  const days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  return monthIndex === 1 ? days[monthIndex] + (isLeapYear(Y) ? 1 : 0) : days[monthIndex];
}

function isLeapYear(Y) {
  return Y % 4 === 0;
}
// find first sunday of the month
// 1st Sunday of the year
// last monday of the month
function solution(Y, A, B, W) {
  const weekDayIndexYearStarts = WEEK_DAYS.indexOf(W);

  const monthIndexVacationStarts = MONTHS.indexOf(A);
  let daysVacationStarts = 1;
  for (let i = 0; i < monthIndexVacationStarts; i++) {
    daysVacationStarts += getMonthDays(Y, i);
  }
  const weekDayIndexVacationBegins =
    (weekDayIndexYearStarts + ((daysVacationStarts % 7))) % 7;

  // this will be first Sunday of the Month
  const daysFirstFlight = daysVacationStarts + (7 - weekDayIndexVacationBegins);

  const monthIndexVacationEnds = MONTHS.indexOf(B);
  let daysVacationEnds = 1;
  for (let i = 0; i <= monthIndexVacationEnds; i++) {
    daysVacationEnds += getMonthDays(Y, i);
  }
  console.log({ daysVacationEnds, daysFirstFlight, daysVacationStarts, weekDayIndexYearStarts, weekDayIndexVacationBegins });

  return Math.round((daysVacationEnds - daysFirstFlight) / 7);
}

console.log(solution(2020, "March", "May", "Wednesday"));