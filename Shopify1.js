/**
 * array of strings "productName, PopularityScoreAsaStringOutOf100, and priceIntegerAsaString"
 * how would you rank the items by popularity.
 * If there is a tie with pop score, place the cheaper priced item first.
 *
 * I was asked to build a URL Shortener in 1hr
 *
 * Write a function to validate the checksum in a UPC code.
 *
 * Given a list of items and quantities, apply discount rules and determine the cart's checkout value.
 */

const sampleArray = ["coco,6,89.9", "ncjd,8,90", "coco,6,70"];

function sortArray(sampleArray) {
  return sampleArray.sort((a, b) => {
    const [productNameA, popularityScoreA, priceA] = a.split(",");
    const [productNameB, popularityScoreB, priceB] = b.split(",");
    if (popularityScoreA > popularityScoreB) {
      return 1;
    } else if (popularityScoreA < popularityScoreB) {
      return -1;
    }

    if (popularityScoreA === popularityScoreB) {
      if (priceA < priceB) {
        return 1;
      } else {
        return -1;
      }
    }
  });
}
sampleArray.sort((a, b) => {
  const [productNameA, popularityScoreA, priceA] = a.split(",");
  const [productNameB, popularityScoreB, priceB] = a.split(",");
  if (popularityScoreA > popularityScoreB) {
    return 1;
  } else if (popularityScoreA < popularityScoreB) {
    return -1;
  }

  if (popularityScoreA === popularityScoreB) {
    if (priceA < priceB) {
      return 1;
    } else {
      return -1;
    }
  }
});

console.log(data);
