// function solution(A) {
//   let i = 0;
//   let j = 0;
//   let sum = A[0];
//   while (i !== A.length - 1) {
//     let localMax = 0;
//     j = i + 1;
//     console.log({ i, localMax, sum });
//     while (j < i + 6 && j < A.length) {
//       if (A[j] <= 0 && j !== A.length) {
//         j++;
//         continue;
//       }
//       localMax = A[j];
//       j++;
//       console.log({ j });
//       break;
//     }

//     sum += localMax;

//     i += localMax.indexOf(sum) + 1;
//     console.log("here", { i, j, localMax, sum });
//   }

//   return sum;
// }

// console.log(solution([1, -2, 0, 9, -1, -2]));
function solution(A) {
  const dp = [A[0]];
  for (let i = 1; i < A.length; i++) {
    let candidates = [];
    for (let j = 1; j <= 6; j++) {
      if (i - j >= 0) {
        // console.log(i - j);
        candidates.push(dp[i - j] + A[i]);
      }
    }
    // console.log(candidates)
    dp[i] = Math.max(...candidates);
    console.log(dp)
  }

  return dp[A.length - 1];
}
console.log(solution([1, -2, 0, 9, -1, -2]));
