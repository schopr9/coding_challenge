/**
 * 
 * @param {int} x coordinates 
 * @param {int} y coordinate
 * @returns {int} minimum Knight moves to reach position x, y
 */
function solution(x, y) {

}

const addMove = (x, y, level) => {
  if ((x >= 0) && (x <= 7) && (y >= 0) && (y <= 7) && board[x][y] == null) {
    board[x][y] = level;
  }
}

class Cell {
  constructor(x, y, distance) {
    this.x = x
    this.y = y
    this.distance = distance
  }
}

function isInside() {
  
}
function findAllPosiblePositions(x, y, level) {
  addMove(x + 1, y + 2, level);
  addMove(x + 2, y + 1, level);
  addMove(x + 2, y - 1, level);
  addMove(x + 1, y - 2, level);
  addMove(x - 1, y - 2, level);
  addMove(x - 2, y - 1, level);
  addMove(x - 2, y + 1, level);
  addMove(x - 1, y + 2, level);
} 