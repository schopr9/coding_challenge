function solution(N) {
  const binary = (N >>> 0).toString(2);
  console.log(binary);
  let maxCount = 0;
  let counter = 0;
  if (!binary) {
    return -1;
  }
  for (const ele of binary.split("")) {
    if (ele == 1) {
      maxCount = Math.max(counter, maxCount);
      counter = 0;
    }

    if (ele == 0) {
      counter++;
    }
  }
  return maxCount;
}

console.log(solution(4));
