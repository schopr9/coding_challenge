/**
 * Given Gary's sequence of up and down steps during his last hike, find and print the number of valleys he walked through.
  For example, if Gary's path is , he first enters a valley  units deep. Then he climbs out an up onto a mountain  units high. Finally, he returns to sea level and ends his hike.
 * @param {Number} n 
 * @param {String} s 
 */
function countingValleys(n, s) {
  let count = 0;
  let isBelowSeaLevel = false;
  s.split("").reduce((sum, step) => {
    step === "U" ? sum++ : sum--;

    if (sum < 0) {
      isBelowSeaLevel = true;
    }

    if (sum === 0 && isBelowSeaLevel) {
      isBelowSeaLevel = false;
      count = count + 1;
    }
    return sum;
  }, 0);
  return count;
}

console.log(countingValleys(5, "UDDDUDUU")); // sample output 1
