function isBalanced(s) {
  const bracketMapper = {
    "{": "}",
    "[": "]",
    "(": ")",
  };
  const unmatchedArray = [];
  for (const value of s) {
    if (bracketMapper[value]) {
      unmatchedArray.push(value);
    } else if (value === bracketMapper[unmatchedArray.pop()]) {
      continue;
    } else {
      return "NO";
    }
  }
  return unmatchedArray.length === 0 ? "YES" : "NO";
}

console.log(isBalanced("{(([])[])[]]}"));
