function solution(N, A, B) {
    // is edge[i] connected to edge[i + 1]
    const isConnected = new Array(N).fill(false);
  
    for (let i = 0; i < A.length; i++) {
      isConnected[A[i]] = isConnected[A[i]] || (A[i] + 1 === B[i]);
      isConnected[B[i]] = isConnected[B[i]] || (B[i] + 1 === A[i]);
    }
  
    let result = true;
    for (let i = 1; i < N; i++) {
      result = result && isConnected[i];
    }
  
    return result;
  }

  console.log(solution(4, [1, 2, 4, 4, 3], [2, 3, 1, 3, 1]));
  console.log(solution(4, [1, 2, 1, 3], [2, 4, 3, 4]));
  console.log(solution(6, [2, 4, 5, 3], [3, 5, 6, 4]));
  console.log(solution(3, [1, 3], [2, 2]));