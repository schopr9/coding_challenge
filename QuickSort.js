/**
 * partition alorithm
 * 
 * 1. Pick the last element as pivot
 * 2. i and j are 2 counter which will check if array[j] <= pivot
 */

function partition(array, start, end) {
  let pivot = array[end];
  let i = start;

  for (let j=start; j <= end -1; j++) {    
    if (array[j] <= pivot) {
      [array[i], array[j]] = [array[j], array[i]]
      i += 1;
    }
  }
  [array[i], array[end]] = [array[end], array[i]]  
  return i; 
}    
    
/**
 * This will be called recursively until the array is sorted
 * @param {Array} array 
 * @param {number} start 
 * @param {number} end 
 */ 
function quickSort(array, start, end) {
  if (start < end) {
    pivotIndex = partition(array, start, end);
    quickSort(array, start, pivotIndex - 1);
    quickSort(array, pivotIndex+1, end)
  }
  console.log('array', array); 
}
quickSort([12, 9, 7, 15, 10], 0, 4)