function Business(name, location, id) {
  this.name = name;
  this.location = location;
  this.id = id;
}

function Chain(name, amount) {
  this.name = name;
  this.amount = amount;
}

function getChains(businesses, location) {
  // solution here
  const filterBusinesses = businesses.filter(
    business => business.location === location
  );
  return filterBusinesses
    .reduce((output, business) => {
      if (output.map(a => a.name).includes(business.name)) {
        output.find(a => a.name === business.name).amount += 1;
      } else {
        output.push(new Chain(business.name, 1));
      }
      return output;
    }, [])
    .sort((a, b) => b.name < a.name);
}

// test:
const businesses = [
  new Business("Starbucks", "Toronto", 101),
  new Business("Starbucks", "Toronto", 102),
  new Business("Starbucks", "Toronto", 101),
  new Business("Tim Hortons", "Toronto", 97),
  new Business("Walmart", "Calgary", 5),
  new Business("Metro", "Calgary", 222)
];

const chains = getChains(businesses, "Toronto");
console.log(chains);

// expected results by: getChains(businesses, 'Toronto')
[new Chain("Tim Hortons", 1), new Chain("Starbucks", 2)];

// expected results by getChains(businesses, 'Calgary')
[new Chain("Walmart", 1), new Chain("Metro", 1)];
