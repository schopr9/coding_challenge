class SpaceShip {


  constructor(x, y) {
    this.x = x
    this.y = y
    this.speed = 0
  }

  coordinates() {
    return [this.x, this.y]
  }

  onPress(input){

    if (input === 'A') {
      this.x -= 2
    } else if (input === 'S') {
      this.decreaseSpeed()
      this.y -= 2
    } else if (input === 'W') {
      this.increaseSpeed()
      this.y += 2
    } else if (input === 'D') {
      this.x += 2
    }

    if (this.y === 250) {
      return "We have reached the Moon"
    }

    return this.coordinates()
  }

  increaseSpeed() {
    this.speed = Math.min(5, this.speed + 2)
    return this.speed
  }

  decreaseSpeed() {
    this.speed = Math.max(1, this.speed - 2)
    return this.speed
  }
}


let newSpaceShip = new SpaceShip(0, 0)
newSpaceShip.onPress('A')
console.log(newSpaceShip.onPress('A'))
console.log(newSpaceShip.onPress('W'))
console.log(newSpaceShip.onPress('W'))