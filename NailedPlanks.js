function solution(A, B, C) {
  if (Math.min(C) > Math.max(A) && Math.min(C) > Math.max(B)) return -1;
  let numberOfPlanks = A.length;
  let counter = 0;
  let dp = Array.from(Array(C.length).keys());

  for (const nail of C) {
    for (const val in dp) {
      if (nail >= A[val] && nail <= B[val] && numberOfPlanks != 0) {
        numberOfPlanks--;
        counter++;
        const index = dp.indexOf(Number(val));
        if (index !== -1) {
          dp.slice(index, 1);
        }
      }
    }
  }

  if (numberOfPlanks === 0) {
    return counter;
  }
  return -1;
}

console.log(solution([1, 4], [2, 6], [2, 5]));
