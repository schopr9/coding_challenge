const finalPrice = input => {
  let processed = [];
  let fullPrice = [];
  let totalCost = 0;
  for (const [i, obj] of input.entries()) {
    if (processed.includes(i)) {
      continue;
    }

    if (obj >= Math.min(...input.slice(i + 1))) {
      totalCost += obj - (input[i + 1] - obj);
      totalCost += input[i + 1] - (input[i + 1] - obj);
      processed.push(i + 1);
    } else {
      totalCost += obj;
      fullPrice.push(i);
    }
  }
  console.log(totalCost);
  console.log(fullPrice);
};

finalPrice([2, 3, 1, 2, 4, 2]);
