/**
 * 
 * @param {Input: s = "leetcode"
    Output: 2
    Explanation: The substring "ee" is of length 2 with the character 'e' only.} s 
 */

/**
 * slide window algorithm
 * start = 0
 * end = 1
 * change start when start is not equal to end
 * until then end - start + 1 gives the maximum window
 * @param {string} slide window algorithm
 * @return {number}
 */
var maxPower = function(s) {

  let start = 0
  let power = 1
  for (let end = 1; end < s.length; end ++) {
    if (s[start] !== s[end]) {
      start = end
    }

    power = Math.max(power, end - start + 1)
    console.log(power)
  }

  return power

};

console.log(maxPower('leetcode'))