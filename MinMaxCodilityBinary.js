function solution(numberOfBlocks, maxNumber, array) {
  let begin = array.reduce((a, b) => a + b, 0); // Calculate total sum of A
  begin = Math.ceil(begin / numberOfBlocks); // Calculate the mean of each theorethical block
  begin = Math.max(begin, Math.max(...array)); // Set begin to the highest number in array if > than the mean

  // In short: begin is now the smallest possible block sum

  // Calculate largest possible block sum
  let end = begin + maxNumber + 1;
  var result = 0;

  console.log({ begin, end });

  while (begin <= end) {
    // Calculate the midpoint, which is our result guess
    const midpoint = (begin + end) / 2;

    let currentBlockSum = 0;
    let block = 1;


    for (let number of array) {
      currentBlockSum += number;

      // If currentBlockSum > midpoint means that we are
      // in a different block...
      if (currentBlockSum > midpoint) {
        ++block;

        // ...so we reset sum with the current number
        currentBlockSum = number;
        console.log({ block });

        // but if we are out of blocks, our guess (midpoint) is incorrect
        // and we will have to adjust it
        if (block > numberOfBlocks) break;
      }
    }

    // If we are out of blocks
    // it means that our guess (midpoint) is bigger than we thought
    if (block > numberOfBlocks) {
      begin = midpoint + 1;
      // else, it's smaller
    } else {
      console.log({ midpoint });
      result = midpoint;
      end = midpoint - 1;
    }
  }

  return result;
}

console.log(solution(3, 5, [2, 1, 5, 1, 2, 2, 2]));
