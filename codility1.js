function solution(A, B, P) {
  if (!A.length || !B.length || !P) return 'NO CONTACT'
  let directory = [];
  for (const [index, val] of A.entries()) {
    directory.push({ name: val, number: B[index] });
  }
  directory.sort(function (a, b) {
    if (a.name < b.name) {
      return -1;
    }
    if (a.name > b.name) {
      return 1;
    }
    return 0;
  });
  for (const val of directory) {
    if (val.number.includes(P) !== -1) {
      return val.name
    }
  }

  return 'NO CONTACT'
}

console.log(solution(["b", "a"], ["654656", "56464"], "56"));