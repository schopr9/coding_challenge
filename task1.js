/**
 * findWord(["P>E","E>R","R>U"]) 
 */

 function findWord(arr) {
  let obj = {}

  for (let val of arr) {
    const ele = val.split('>')
    obj[ele[0]] = ele[1]
  }

  let starting = Object.keys(obj)
  let ending = Object.values(obj)

  let start = starting.filter(val => !ending.includes(val))

  let res = ''

  while(start.length) {
    let ele = start.pop()
    res += ele
    obj[ele] && start.push(obj[ele])
  }

  return res

}

 console.log(findWord(["P>E","E>R","R>U"]))
 console.log(findWord(["U>N", "G>A", "R>Y", "H>U", "N>G", "A>R"]))
 console.log(findWord(["I>F", "W>I", "S>W", "F>T"]))
 console.log(findWord(["R>T", "A>L", "P>O", "O>R", "G>A", "T>U", "U>G"]))
 console.log(findWord(["W>I", "R>L", "T>Z", "Z>E", "S>W", "E>R", "L>A", "A>N", "N>D", "I>T"]))