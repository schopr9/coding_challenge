/**
 * @param {string} s
 * @param {string} p
 * @return {boolean}
 */
var isMatch = function (s, p) {
  if (!p) {
    console.log({ s, p });
    return !s;
  }

  const firstCharMatch = p[0] === "." || s[0] === p[0];

  if (p[1] === "*") {
    console.log({ s, p });
    return isMatch(s, p.slice(2)) || (firstCharMatch && isMatch(s.slice(1), p));
  }

  return firstCharMatch ? isMatch(s.slice(1), p.slice(1)) : false;
};

console.log(isMatch("aa", "a*"));
