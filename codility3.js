const WEEK_DAYS = [
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
  "Sunday",
];

const MONTHS = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

function getMonthDays(Y, monthIndex) {
  const days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  return monthIndex === 1 ? days[monthIndex] + (isLeapYear(Y) ? 1 : 0) : days[monthIndex];
}

function isLeapYear(Y) {
  return Y % 4 === 0;
}
// find first sunday of the month
// find last monday of the month
// get the difference of dates divided by 7

function solution2(Y, M, E, D) {
  const getFirstSunday = firstSunday(Y, M);
  const getLastMonday = lastMonday(Y, E);
  console.log({ getFirstSunday, getLastMonday });
  return Math.round(
    (getLastMonday - getFirstSunday) / (7 * 24 * 60 * 60 * 1000)
  );
}

function firstSunday(month, year) {
  let tempDate = new Date(`${year} ${month} 1`);
  let day = tempDate.getDay();
  let toNextSun = day !== 0 ? 7 - day : 0;
  tempDate.setDate(tempDate.getDate() + toNextSun);

  return tempDate;
}

function lastMonday(Y, M) {
  const lastDay = getMonthDays(Y, MONTHS.indexOf(M));
  let tempDate = new Date(`${Y} ${M} ${lastDay}`);
  const day = tempDate.getDay();

  let toLastMonday = 0;
  if (day > 1) {
    toLastMonday = day - 1;
  } else if (day === 0) {
    toLastMonday = 7;
  }
  console.log({ tempDate, toLastMonday, lastDay });
  tempDate.setDate(tempDate.getDate() - toLastMonday);
  return tempDate;
}
console.log(solution2(2020, "March", "May", "Wednesday"));
