/**
 * Bubble sort algorithm
 * compare the adjacent numbers and swap if left one is greater
 * @param {Array} array
 */

function bubbleSort(array) {
  for (let i = 0; i < array.length; i++) {
    for (let j = 0; j < array.length - 1; j++) {
      if (array[j] > array[j + 1]) {
        [array[j], array[j + 1]] = [array[j + 1], array[j]];
      }
    }
  }
  console.log(array);
}

function optimizedBubbleSort(array) {
  let isSorted = false;
  let lastUnsorted = array.length - 1;

  while (!isSorted) {
    isSorted = true;
    for (let j = 0; j < lastUnsorted; j++) {
      if (array[j] > array[j + 1]) {
        [array[j], array[j + 1]] = [array[j + 1], array[j]];
        isSorted = false;
      }
    }
    lastUnsorted--;
  }
  console.log(array);
}
optimizedBubbleSort([12, 9, 7, 15, 10]);
